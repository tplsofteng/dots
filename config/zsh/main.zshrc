. ~/.config/shell/shellrc

setopt prompt_subst
setopt correct
setopt histignorealldups    # Duplicate command?, remove the older one

# Turn off all types of beeps
unsetopt BEEP

# Color definitions
GREEN='%F{green}'
BLUE='%F{blue}'
YELLOW='%F{yellow}'
RESET='%f'

export PS1='(%n@%M %~) ${YELLOW} $(parse_git_branch) ${RESET}
$'

bindkey -v # vi mode, some plugins need this enabled before sourcing

STD_SYNTAX_FILE=/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
ARCH_SYNTAX_FILE=/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

[ -f "$STD_SYNTAX_FILE" ] && ZSH_HIGHLIGHTER="$STD_SYNTAX_FILE"
[ -f "$ARCH_SYNTAX_FILE" ] && ZSH_HIGHLIGHTER="$ARCH_SYNTAX_FILE"
[ -f "$ZSH_HIGHLIGHTER" ] && source "$ZSH_HIGHLIGHTER"

ARCH_AUTOSUGGEST_FILE="/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
STD_AUTOSUGGEST_FILE="/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh"

[ -f "$STD_AUTOSUGGEST_FILE" ] && ZSH_AUTOSUGGEST="$STD_AUTOSUGGEST_FILE"
[ -f "$ARCH_AUTOSUGGEST_FILE" ] && ZSH_AUTOSUGGEST="$ARCH_AUTOSUGGEST_FILE"
[ -f "$ZSH_AUTOSUGGEST" ] && source "$ZSH_AUTOSUGGEST"

# enable suggestions
autoload -Uz compinit
compinit

bindkey -s '^xf' 'fzf^M'
bindkey -s '^xc' 'fuzz_cd^M'
bindkey -s '^xv' 'fuzz_edit^M'
bindkey -s '^xr' 'shell_reload^M'
bindkey -s '^xg' 'reset^M'
bindkey -s '^xs' 'tes^M'
