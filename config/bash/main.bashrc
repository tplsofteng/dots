. ~/.config/shell/shellrc

# enable vi mode editing in bash
set -o vi
shopt -s cdspell
shopt -s dirspell 
shopt -s checkwinsize

export PS1='(\u@\h \w) \[\033[01;33m\] $(parse_git_branch) \[\033[0m\]
$'

# key bindings
bind -x '"\C-xf": fzf'
bind -x '"\C-xc": fuzz_cd;history -r'
bind -x '"\C-xv": fuzz_edit;history -r'
bind -x '"\C-xr": shell_reload'
bind -x '"\C-xg": reset'
bind -x '"\C-xs": tes'
