" Instruct nvim to use existing vim config
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath=&runtimepath
source ~/.config/vim/main.vimrc
