# My dotfiles
bash, vim, sxhkd, bspwm etc. + collection of utility scripts
Mainly used on arch, debian and centos/rhel

# Install
Run the install script to source config files and create dynamic links to others.
```
./install.sh
./install.sh -r (to remove)
```

# Dependencies
optional python cogapp tool for templating scripts:
`pacman -S python-cogapp`
