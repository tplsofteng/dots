#!/usr/bin/env python3
# Script to dump chrome json bookmarks backups
import json
import sys

def iterate_children(node):
    if "uri" in node:
        print(node.get("uri"))

    if "children" in node:
        for child in node["children"]:
            iterate_children(child)

if len(sys.argv) < 2:
    print("Usage: python dump_bookmarks_json.py <bookmarks_file>")
    sys.exit(1)

filename = sys.argv[1]

with open(filename, "r") as file:
    document = json.load(file)
    iterate_children(document)
