#!/usr/bin/python
import re
import sys
import html

def vtt_to_text(input_file, output_file):
    try:
        with open(input_file, 'r') as f:
            vtt_content = f.read()

            vtt_content = re.sub(r'&amp;#39;','\'',vtt_content)
            
            vtt_content = html.unescape(vtt_content)

            # Remove WEBVTT header if present
            vtt_content = re.sub(r'^WEBVTT\s*\n*', '', vtt_content)

            # Remove timestamps and formatting
            vtt_content = re.sub(r'<[^>]*>', ' ', vtt_content)

            # Remove excess whitespace
            vtt_content = re.sub(r'\s+', ' ', vtt_content)

            # Write the processed content to the output file
            with open(output_file, 'w') as out_f:
                out_f.write(vtt_content.strip())

        print(f"Conversion successful. Text written to {output_file}")
    except Exception as e:
        print(f"Error converting VTT to text: {e}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python vtt_to_text.py input.vtt output.txt")
    else:
        input_file = sys.argv[1]
        output_file = sys.argv[2]
        vtt_to_text(input_file, output_file)
